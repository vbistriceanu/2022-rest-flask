import pytest
from src.all_lamps import all_lamps
from src.lamp_manager import LampManager


class TestLampManager:
    def setup_method(self):
        self.lm = LampManager()
        self.alc = len(self.lm.get_lamps())

    def teardown_method(self):
        all_lamps.clear()
        self.lm = None

    def test_that_initially_there_are_no_lamps(self):
        assert self.alc == 0

    def test_that_adding_a_lamp_increases_the_all_lamps_count_by_one(self):
        self.lm.add_lamp()
        actual = len(self.lm.get_lamps())
        expected = self.alc + 1
        assert actual == expected

    def test_that_removing_lamp_after_adding_lamp_leaves_the_lamp_count_unchanged(self):
        lmp = self.lm.add_lamp()
        self.lm.remove_lamp(lmp.id)
        assert len(self.lm.get_lamps()) == self.alc

    def test_get_lamp_by_id(self):
        lid = -1
        lmp = self.lm.add_lamp(ident=lid)
        assert lmp is self.lm.get_lamp_by_id(lid)