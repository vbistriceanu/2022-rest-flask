import pytest
from src.lamp import Lamp
import uuid


class TestLampMethods:
    def setup_method(self):
        self.tl = Lamp()

    def test_that_default_lamp_is_off(self):
        assert self.tl.is_on is False

    def test_that_default_lamp_can_be_turned_on(self):
        self.tl.turn_on()
        assert self.tl.is_on is True

    def test_that_a_default_lamp_is_off_after_being_turned_on_and_off(self):
        self.tl.turn_on()
        self.tl.turn_off()
        assert self.tl.is_on is False

    def test_that_the_id_of_a_default_lamp_doesnt_match_another_id(self):
        other_id = str(uuid.uuid4())
        assert self.tl.id != other_id

    def test_non_default_lamp_has_the_id_passed_in_as_argument_and_correct_status(self):
        tl = Lamp(ident=-1, status=True)
        assert tl.id == -1
        assert tl.is_on is True