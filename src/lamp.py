import uuid


class Lamp:
    def __init__(self, ident=None, status=False):
        if ident is None:
            self.id = str(uuid.uuid4())
        else:
            self.id = ident
        self.is_on = status

    def __repr__(self):
        rep = "<Lamp(lid={self.id!r})>".format(self=self)
        print(rep)
        return rep

    def turn_on(self):
        self.is_on = True

    def turn_off(self):
        self.is_on = False

    def matches(self, lid):
        return self.id == lid
