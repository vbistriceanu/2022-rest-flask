from marshmallow import Schema, fields


class LampSchema(Schema):
    id = fields.Str()
    is_on = fields.Boolean(data_key='ison')
