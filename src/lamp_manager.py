from src.lamp import Lamp
from src.all_lamps import all_lamps


class LampManager:
    def __init__(self):
        pass

    def add_lamp(self, ident=None, status=False):
        lmp = Lamp(ident, status)
        all_lamps.append(lmp)
        return lmp

    def get_lamps(self):
        return all_lamps

    def get_lamp_by_id(self, lid):
        for i in range(len(all_lamps)):
            if all_lamps[i].id == lid:
                return all_lamps[i]
            else:
                return None
        return None

    def update_lamp(self, lid, status):
        lmp = self.get_lamp_by_id(lid)
        if lmp is None:
            return False
        else:
            lmp.is_on = status
        return True

    def remove_lamp(self, lid):
        for i in range(len(all_lamps)):
            if all_lamps[i].id == lid:
                all_lamps.pop(i)
                return True
            else:
                return False
        return False
