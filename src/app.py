from flask import Flask, request, jsonify
from markupsafe import escape
from http import HTTPStatus

from src.lamp_manager import LampManager
from src.mm_definitions import LampSchema

app = Flask(__name__)
app.config['TESTING'] = True

@app.route("/lamps", methods=["GET"])
def get_all_lamps():
    print("Inside GET /lamps")
    lamps = LampManager().get_lamps()
    schema = LampSchema(many=True)
    response = jsonify(schema.dump(lamps))
    response.status_code = HTTPStatus.OK
    response.headers["Content-Type"] = "application/json"
    return response


@app.route("/lamps/<lid>", methods=["GET"])
def get_lamp(lid):
    lmp = LampManager().get_lamp_by_id(lid)
    if lmp is None:
        reason = "Not found: " + str(lid)
        return handle_NOT_FOUND(reason)
    else:
        schema = LampSchema()
        response = jsonify(schema.dump(lmp))
        response.status_code = HTTPStatus.OK
        response.headers["Content-Type"] = "application/json"
    return response


@app.route("/lamps", methods=["POST"])
def create_lamp():
    _json = request.json
    if not _json:
        handle_BAD_REQUEST("Bad request: no data in the POST request body")
        return request
    _id = _json['id']
    _ison = _json['ison']
    lmp = LampManager().add_lamp(ident=None, status=_ison)
    schema = LampSchema()
    response = jsonify(schema.dump(lmp))
    response.status_code = HTTPStatus.CREATED
    response.headers["Content-Type"] = "application/json"
    response.headers['location'] = '/lamps/' + lmp.id
    response.autocorrect_location_header = True
    return response


@app.route("/lamps/<lid>", methods=["PUT"])
def update_lamp(lid):
    _json = request.json
    if not _json:
        handle_BAD_REQUEST("Bad request: no data in the POST request body")
        return request
    _id = _json['id']
    _ison = _json['ison']
    if not LampManager().update_lamp(lid, _json):
        reason = "Not found: " + str(lid)
        return handle_NOT_FOUND(reason)
    else:
        response = jsonify()
        response.status_code = HTTPStatus.NO_CONTENT
        response.headers["Content-Type"] = "application/json"
    return response


@app.route("/lamps/<lid>", methods=["DELETE"])
def delete_lamp(lid):
    if not LampManager().remove_lamp(lid):
        reason = "Not found: " + str(lid)
        return handle_NOT_FOUND(reason)
    else:
        response = jsonify()
        response.status_code = HTTPStatus.NO_CONTENT
        response.headers["Content-Type"] = "application/json"
    return response


@app.errorhandler(HTTPStatus.NOT_FOUND)
def handle_NOT_FOUND(reason=None):
    message = {
        'status': 404,
        'reason': reason,
    }
    response = jsonify(message)
    response.status_code = HTTPStatus.NOT_FOUND
    response.headers["Content-Type"] = "application/json"
    return response


@app.errorhandler(HTTPStatus.BAD_REQUEST)
def handle_BAD_REQUEST(reason=None):
    message = {
        'status': 405,
        'reason': reason,
    }
    response = jsonify(message)
    response.status_code = HTTPStatus.BAD_REQUEST
    response.headers["Content-Type"] = "application/json"
    return response


if __name__ == '__main__':
    # Avoid port 5000 on Macs, it's being used by AirPlay
    app.run(host="localhost", port=5001, debug=True)
