# README #

The repo contains setup instruction from a simple REST application implemented using frameworks and libraries
from the Python ecosystem.

### What is this repository for? ###

The project is setup to use Python 3.8+ running in a Linux environment
(Ubuntu 20.04).

Here are the other relevant frameworks/libraries:

* Packaging and project dependencies are managed using `pipenv`
* `Flask` is used for to manage the REST end points.
* Serialization/deserialization is done using `marshmallow`.

### How do I get set up? ###

(i) Verify that your have Python 3 installed

If you start with a fresh installation of Ubuntu 20.04 then you should
be ok as Python 3 is already available.
```
$ python3 --version
Python 3.8.10
```

(ii) Verify if `pip3` available

If you start with a fresh installation of Ubuntu 20.04 then `pip3`
doesn't come pre-installed.
```
$ pip3 --version
Command 'pip3' not found, but can be installed with:
sudo apt install python3-pip
```

(iii) Install `pip3`
```
$ sudo apt update
$ sudo apt -y install python3-pip
```
Verify that `pip3` has been installed:
```
$ pip3 --version
pip 20.0.2 from /usr/lib/python3/dist-packages/pip (python 3.8)
```

(iv) Install `pipenv`
```
$ pip3 install --user pipenv
```
The `--user` option for `pip3` indicates a local installation to
prevent the messing with global python packages.

Now verify that pipenv has been properly installed:
```
$ source ~/.profile
$ pipenv graph
Warning: No virtualenv has been created for this project yet!
Consider running `pipenv install` first to automatically
generate one for you or see `pipenv install --help` for further instructions.
```

(v) Clone the project repo
```
$ cd && mkdir python-projects
$ cd python-projects
$ git clone https://cs445@bitbucket.org/vbistriceanu/2022-rest-flask.git

```

(vi) Install project dependencies
```
$ cd 2022-rest-flask
$ pipenv install
```

(vii) Activate the project virtual environment
```
$ pipenv shell
```
When you're done with the work you do in the virtual environment just type `exit`
to go back to where you were before running `pipenv shell`

(viii) Run the REST application
```
$ export FLASK_ENV=development
$ python -m src.app
 * Serving Flask app 'app' (lazy loading)
 * Environment: development
 * Debug mode: on
 * Running on http://localhost:5001 (Press CTRL+C to quit)
 * Restarting with stat
 * Debugger is active!

```
NOTE: Normally the port used by Flask is 5000; this example uses port
5001 to prevent a situation where you'd be getting errors when
running the code on a Mac where port 5000 is used by AirPlay.

(ix) Verify that the application responds to REST requests
```
$ curl -i --request GET 'http://localhost:5001/lamps'
HTTP/1.1 200 OK
Server: Werkzeug/2.1.2 Python/3.8.10
Date: Sun, 08 May 2022 15:03:18 GMT
Content-Type: application/json
Content-Length: 3
Connection: close

[]
```

(x) Run tests
```
$ cd ~/python-projects/2022-rest-flask
$ pytest
==================== test session starts ====================
platform linux -- Python 3.8.10, pytest-7.1.2, pluggy-1.0.0
rootdir: /home/virgil/python-projects/2022-rest-flask
collected 9 items                                                                                                                                       

tests/test_lamp.py .....                             [ 55%]
tests/test_lamp_manager.py ....                      [100%]

===================== 9 passed in 0.01s =====================
```

### Run the Postman test suite ###

* Launch Postman. If this is the first time you're using the tool,
  select "Workspaces" from the menu on the upper left
  and then press "Create Workspace". Name the workspace something
  meaningful, such as "cs445 REST Lamp Project", select "Personal"
  for Visibility and then press "Create Workspace".
* Select the newly created workspace from the "Workspaces" menu.
  The name of the workspace will be visible in the upper left corner,
  below the Home menu item.
* Go to "File > Import", select "Link" from the top of the box, enter
  the following link: https://www.getpostman.com/collections/ca3291ca4ea9aa897206
  and then press "Continue" and "Import". On the left pane select
  the "Collections" tab: you should see a collection called "REST Lamp Tests".
* Create an environment in which to run the automated suite:
    + Press the "New" button in the upper left corner of the main Postman'
      s screen and select "Environment" for the menu.
    + Choose a name for the new environment, such as
      "cs445 REST Lamp - localhost" and create a variable named
      `base_url` of type 'default' and with the Initial and Current Values
      of`http://localhost:5001`, then press "Save".
* Start the server that accepts and responds to REST requests for your
  application (see step (viii) above). The Postman test scripts will be
  running against the base URL defined by the `base_url` variable.
* Run the automated test suite:
    + Select the "REST Lamp Tests" from Collections.
    + Hover over the "REST Lamp Tests" name, press the ellipsis on the right
      of the name, and select "Run Collection" from that menu. Make sure
      the "cs445 REST Lamp - localhost" is selected from the pull down
      menu in the upper right.
    + Click on the blue "Run REST Lamp Tests" to find out how many
      of the tests are passing.

### Notes ###

* In this sample project `marshmallow` is only used to serialize Python objects to a JSON
  representation. The complexity of this demo project is so low that
  deserializing objects does not use `marshmallow`.
    + More information about Marshmallow
      [here](https://marshmallow.readthedocs.io/en/stable/quickstart.html#serializing-objects-dumping)
* You should never ever run a production application with the setup
  described in this README file.  That's primarily for two reasons:
    + The embedded Flask web server (Werkzeug) cannot handle any meaningful
      production load.
    + From a security point of view you should not run a debugger
      in a production environment because it allows way too much access to
      the application code.

If you want to learn more about a production ready Flask application here's
a brief [introduction](https://www.toptal.com/flask/flask-production-recipes)


### Who do I talk to? ###

* Email bistriceanu@iit.edu
